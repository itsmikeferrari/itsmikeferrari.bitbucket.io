var app = angular.module("scFineArt", ["ngRoute"]);
// CONFIG
//app.config(['$routeProvider', function($routeProvider){
//    $routeProvider
//        .when('/',{template:'This is the default Route'})
//        .when('/san-diego-art-collection',{template:'san-diego-art-collection'})
//        .when('/friends-artists-colleagues',{template:'friends-artists-colleagues'})
//        .otherwise({redirectTo:'/'});
//}]);
//app.config(['$routeProvider', function($routeProvider) {
//  $routeProvider.
//    when('/', { 
//      templateUrl: 'pages/main.html'
//    }).
//    when('/san-diego-art-collection', { 
//      templateUrl: 'pages/san-diego-art-collection.html'
//    }).
//    when('/friendsartistscolleagues', { 
//      templateUrl: 'pages/friends-artists-colleagues.html'
//    }).
//    otherwise({
//      redirectTo: '/' });
//}]);

app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "pages/main.html"
        })
        .when("/san-diego-art-collection", {
            templateUrl: "pages/san-diego-art-collection.html"
        })
        .when("/s-cohen-fine-art-client-reviews", {
            templateUrl: "pages/s-cohen-fine-art-client-reviews.html"
        })
        .when("/about-s-cohen-fine-art", {
            templateUrl: "pages/about-s-cohen-fine-art.html"
        })
        .when("/san-diego-art-restoration", {
            templateUrl: "pages/san-diego-art-restoration.html"
        })
        .when("/san-diego-art-appraisal", {
            templateUrl: "pages/san-diego-art-appraisal.html"
        })
        .when("/friendsartistscolleagues", {
            templateUrl: "pages/friends-artists-colleagues.html"
        })
        .otherwise({
            template: "<h1>None</h1><p>Nothing has been selected</p>"
        });
});

//app.config(function($routeProvider) {
//    $routeProvider
//    .when("/", {
//        templateUrl : "pages/main.html"
//    })
//    .when("/friendsartistscolleagues", {
//        templateUrl : "friends-artists-colleagues.html"
//    })
//    .when("/green", {
//        templateUrl : "green.htm"
//    })
//    .when("/blue", {
//        templateUrl : "blue.htm"
//    });
//});


// CONTROLLERS
app.controller("scfaCtrl", function ($scope) {

    $scope.$on('eventlaunch', function (scope) {
        
        $('.materialboxed').materialbox();
        //         console.log("recd emitter");         
        $('.slider').slider();
        //            $('.carousel').carousel();
//        $('.carousel.carousel-slider').carousel({
//            fullWidth: true
//        });
        $('.modal').modal();
        $('ul.tabs').tabs();
        // Materialize.toast(message, displayLength, className, completeCallback);
//        Materialize.toast('Notice', 2000) // 4000 is the duration of the toast

        $("#bowtietop").hover(function () {
            $(this).removeClass('delay2');
            $(this).toggleClass("rubberBand");
            $(this).removeClass('rotateIn');

        });

        $(".btnmove").hover(function () {
            $(this).toggleClass("pulse");
        });

        $("#bowtiebottom").hover(function () {
            $("#bowtiebottom").addClass("flip");
            setTimeout(function () {
                $("#bowtiebottom").removeClass("flip");
            }, 2000);
        });
    });

    // When page loaded using ngRoute, trigger the javascript elements
    $scope.$on('$viewContentLoaded', function () {

        $('.slider').slider();
        //            $('.carousel').carousel();
//        $('.carousel.carousel-slider').carousel({
//            fullWidth: true
//        });
        $('.modal').modal();
        $('ul.tabs').tabs();
        // Materialize.toast(message, displayLength, className, completeCallback);
//        Materialize.toast('Notice', 2000) // 4000 is the duration of the toast

        $("#bowtietop").hover(function () {
            $(this).removeClass('delay2');
            $(this).toggleClass("rubberBand");
            $(this).removeClass('rotateIn');

        });

        $(".btnmove").hover(function () {
            $(this).toggleClass("pulse");
        });

        $("#bowtiebottom").hover(function () {
            $("#bowtiebottom").addClass("flip");
            setTimeout(function () {
                $("#bowtiebottom").removeClass("flip");
            }, 2000);


        });



        //        window.scrollTo(0,90);

    }); // END ngRoute trigger


    $scope.people = ["17-10-1971.jpg", "after_the_auction6378180941432720211.jpg", "alexs_and_e_renoir_june_22_-073607289674251993190.jpg", "ann-patricha-mad1014915451795399093.jpg", "an_he845384136419078501.jpg", "bh_hotel_rh931304933037018487.jpg", "cassie_with_abba6480908053824006154.jpg", "claudia_alon_and_vanessa__sol2173497836788013124.jpg", "cr-17873401584105524541.jpg", "cs5222884519595966105.jpg", "david-solomon-paris1994753942855841104773.jpg", "dk_hb__june_16_20086852409712584548551.jpg", "dscf27996487675416383807494.jpg", "image-1072105555247274643.jpg", "image-11510051729526269062.jpg", "image-12892431758155197627.jpg", "image-137891529342136837923.jpg", "image-145886425019673314775.jpg", "image-154562983130489067723.jpg", "image-168971024383448894973.jpg", "image-17143179131210853190.jpg", "image-182673105109539886547.jpg", "image-1969516675164493322.jpg", "image-199004006538320612700.jpg", "image-201814547604214348375.jpg", "image-26150113736187128970.jpg", "image-36766432215939850029.jpg", "image-47980565488142373933.jpg", "image-5564093136704595039.jpg", "image-6584321877735692554.jpg", "image-71920407234805327865.jpg", "image-85957560633898287886.jpg", "image-91456554607838527111.jpg", "image0-101595840738894114882.jpg", "image0-1368864603058911938.jpg", "image0-13858864131996727103.jpg", "image0-155040358476605205379.jpg", "image0-16611366589866246987.jpg", "image0-176925278958735077676.jpg", "image0-183131962995180722624.jpg", "image0-194695755603322014845.jpg", "image0-22843631406847336884.jpg", "image0-3813539450539833110.jpg", "image0-67551379230638260748.jpg", "image0-71788343800797242513.jpg", "image4381143857915068553.jpg", "j-and-s_nov_06_la_jola7873983741831456066.jpg", "la_the_hb5580929220851892902.jpg", "mr-and-mrs-james_fetherolf2934203284979374374.jpg", "m_modeling_firset_design-front6241316210124007113.jpg", "neal-solomon-11-19-028158139373279879469.jpg", "noel_coret4951254463579280313.jpg", "not_yet5000907190933416914.jpg", "paris_sol_3-_june_058912106278727657017.jpg", "paul_valer215640826289911034.jpg", "portrait3731680321033329998.jpg", "s-m-31398483192054324299.jpg", "sc_with_amiry5551330340407353826.jpg", "sc_with_an_he5013787635977946235.jpg", "seth6647579266184122883.jpg", "solomon1094733891272917803.jpg", "sol_in_the_garden135020775555608808.jpg", "stanley_maxwell_brice3389982254756461111.jpg", "yang_zhi-guan_professor_of_guanzhou_academy_of_fine_arts5362909728987935033.jpg"];
    
    // art collection slider
    $scope.artcollection = ["blackandwhite-cohen-after-call.jpg", "cityscape-dsc09513.jpg", "cityscape-dsc09515.jpg", "cityscape-dsc09516.jpg", "cityscape-eugenie-mcevoy-28x36-saint-romain-porvence.jpg", "cityscape-frank-myers-boggs-dankequre-10-5x16-98721-3200-f.jpg", "cityscape-gabriel-dauchot-paris-at-leisure-15-5x31-5-6022-1200-uf.jpg", "cityscape-gabriel-dauchot-spring-at-shore-7-5x18-5-6020-400-uf.jpg", "cityscape-gabriel-dauchot-the-artist-muse-7-5x18-5-6019-400-uf.jpg", "cityscape-gabriel-dauchot-the-rendezvous-7-5x18-5-2707-400-uf.jpg", "cityscape-jean-rigaud-5848-s-15x21-pont-des-arts.jpg", "cityscape-jean-rigaud-5849-s-15x21-pont-nufe.jpg", "cityscape-jean-rigaud-8311-s-13x18.jpg", "cityscape-jean-rigaud-s-21x29-port-neuf-no-frame.jpg", "cityscape-y-le-persan-52-the-grocery-28-5x23-5-1200-n.jpg", "contemporary-1-a2630438748440798620.jpg", "contemporary-1-autocantast6014735938157966291.jpg", "contemporary-2-auto-contrast3864499455695913950.jpg", "contemporary-3-auticantast7974397418642872900.jpg", "contemporary-8443-19x29-j-rexes.jpg", "contemporary-dal-vision-light3133665076210648021.jpg", "contemporary-front-page664222224111983119.jpg", "contemporary-pa0900276055682971878987437.jpg", "contemporary-pa0900343803204158184426197.jpg", "contemporary-pa090036-no-22214518338244761896.jpg", "contemporary-pa090037-auto-contrast5015744940986602189.jpg", "contemporary-pa0900389180659216374418755.jpg", "contemporary-pa090039-auto-contrast8315626483191482038.jpg", "contemporary-pa090040-auto-contrast1044193739476076338.jpg", "contemporary-pa090041-auto-contrast3282284303400399977.jpg", "contemporary-pa090043-auto-contrast6225730944012545734.jpg", "contemporary-pa0900511373273369020327028.jpg", "contemporary-pa090052934974142821023702.jpg", "contemporary-pa090053448364859851015720.jpg", "contemporary-pa0900543389404893384286436.jpg", "contemporary-pa090060498033345510257241.jpg", "contemporary-pa0900612108748873468602855.jpg", "contemporary-pa090066542833912904635796.jpg", "contemporary-pa0900683472358126406834426.jpg", "contemporary-pa0900691278500666748291087.jpg", "contemporary-pa0900709159450961313030791.jpg", "contemporary-pa0900724716382588856674622.jpg", "contemporary-pa0900732469324475718317724.jpg", "figurative-1690-d-anty.jpg", "figurative-1691-d-anty.jpg", "figurative-1692-d-anty.jpg", "figurative-1693-d-anty.jpg", "figurative-1696-d-anty.jpg", "figurative-1698-d-anty.jpg", "figurative-1699-d-anty.jpg", "figurative-1969-15x18-machourek.jpg", "figurative-1995-leon-de-troy-18x16-1857-1955.jpg", "figurative-3006-7x10-l-corbelini.jpg", "figurative-5919-8x10-o-eichinger.jpg", "figurative-5920-8x10-o-eichinger.jpg", "figurative-6039-18x21-j-calogero.jpg", "figurative-6042-18x21-j-calogero.jpg", "figurative-6043-18x21-j-calogero.jpg", "figurative-6045-18x21-j-calogero.jpg", "figurative-6046-18x21-j-calogero.jpg", "figurative-6047-23x28-j-calogero.jpg", "figurative-7905-14x20-jean-paleo-logue.jpg", "figurative-7906-14x20-jean-paleo-logue.jpg", "figurative-8009-35x40-endre-szasz.jpg", "figurative-8010-26x32-endre-szasz.jpg", "figurative-8011-24x329-endre-szasz.jpg", "figurative-80116-40x30-thompson.jpg", "figurative-8012-24x30-endre-szasz.jpg", "figurative-8079-18x22-j-calogero.jpg", "figurative-8136-24x36-robert-stites.jpg", "figurative-8138-30x36-robert-stites.jpg", "figurative-88126-otto-eichinger-8x10.jpg", "figurative-9043-31x16-poncini.jpg", "figurative-9044-21x18-poncini.jpg", "figurative-9047-21x18-poncini.jpg", "figurative-9225-25x31-bernard-locca.jpg", "figurative-9812-24x48-j-retter.jpg", "figurative-a-m-macuourek-60-the-matador-wife-18x15-80139-500-uf.jpg", "figurative-bernard-locca-69-25-5x31-3-4-childrens-welcoming-there-father-oc-500.jpg", "figurative-big-mama.jpg", "figurative-crotto-2499.jpg", "figurative-gabriel-dauchot-the-lover-15-5x31-5-1754-1200-uf.jpg", "figurative-gill-manold-77577size-39x31-3-4.jpg", "figurative-hd-parrot-in-bloom8-5x10-5-1682-300-uf.jpg", "figurative-hd-the-clown-juggler-21-5x18-6000-1200-uf.jpg", "figurative-hd-the-trumpeter-14x10-5-1676-400-uf.jpg", "figurative-j-c-the-flower-nest-21-5x18-6040-800-uf.jpg", "figurative-jc-rooster-and-mask21-5x18-8079-700-uf.jpg", "figurative-jc-the-little-clown-21-5x18-600-uf.jpg", "figurative-jc-the-pink-mask-18x21-5-6043-800-uf.jpg", "figurative-jc-the-sea-shells-21-5x18-6044-800-uf.jpg", "figurative-jean-calogero-oc-f-still-life-with-birds-23-5-x28-3-6047-900.jpg", "figurative-lc-8044.jpg", "figurative-lc-boy-and-bird-10-5-x-7-5-3006-500.jpg", "figurative-lc-boys-with-sombreros-15-x7-5-3005-650-image-only.jpg", "figurative-leon-detroy-pp-entertaining-the-pasha-18x16-f-864-800.jpg", "figurative-luigi-corbellini-3003-s25x21.jpg", "figurative-luigi-corbellini-3077-s-13x9-blue-boy.jpg", "figurative-machdurek-aimi-1875.jpg", "figurative-madeleine-luka-labalancoir-28-5x21-oc-uf-1848-500.jpg", "figurative-uf.jpg", "figurative-yandi-2693size-31-1-2x23-1-2.jpg", "floral-77542-20x24-rouviere.jpg", "floral-77571-15x18m-journod.jpg", "floral-7868-18x21-rouviere.jpg", "floral-8053-skoch.jpg", "floral-elisabete-rouviere-daisies-and-poppies-uf-oc-20x24-77542-300.jpg", "floral-m-journod-77-spring-bouquet-18x15-77571-uf.jpg", "interior-6071-church-interior.jpg", "landscape-1192-23x31-gyula-gero.jpg", "landscape-4929-24x36-a-dzigurski.jpg", "landscape-77495-22x36-dey-de-ribcowsky.jpg", "landscape-77567-23x28-m-journod.jpg", "landscape-77568-18x21-m-journod.jpg", "landscape-77569-19x23-m-journod.jpg", "landscape-77570-15x18m-journod.jpg", "landscape-77572-journod.jpg", "landscape-77573-journod.jpg", "landscape-77597-charles-le-maier-16x20-size.jpg", "landscape-8135-helena-adamoff-7x9.jpg", "landscape-8140-heena-adamoff-7x9.jpg", "landscape-8153-30x40-sether.jpg", "landscape-98789-20x24-hermanus-j-veger.jpg", "landscape-antonio-cortes-uf-oc-a-flock-at-dusk-15x18-9974-5000.jpg", "landscape-copy-of-frank-innocent-6048-st-desert.jpg", "landscape-copy-of-gera-gyula-hungarian-323-5x31-5-oc-uf-the-farm-house-1192-250.jpg", "landscape-dsc09504.jpg", "landscape-dsc09510-0.jpg", "landscape-dsc09514.jpg", "landscape-frank-innocent-6048-st-desert.jpg", "landscape-helena-adamoff-1906-russian-oc-7-5-x-9-5-the-end-of-worry-8139-200.jpg", "landscape-helena-adamoff-1906-russian-oc-7-5-x9-5-the-injured-bird-8140-200.jpg", "landscape-jean-rigaud9924-s-10x14.jpg", "landscape-jean-rigaud9925-s-10-x18-la-mer.jpg", "landscape-kon103-dyf18-21-july-2002-no-frame.jpg", "landscape-louwenhoek-van-veger-20x24-oc-f-98780-500-a-walk-in-the-woods.jpg", "landscape-m-journod-77-avignon-20x2477571-uf.jpg", "landscape-m-journod-77-baux-de-provence-23-5x28-5-77567-uf.jpg", "landscape-m-journod-77-home-in-provence-15x18-77570-uf.jpg", "landscape-m-journod-77-lacoste-provence-13x16-77572-uf.jpg", "landscape-m-journod-77-landscape-of-cavaillon-13x16-77573-uf.jpg", "landscape-m-journod-77-les-baux-de-provence-13x16-77574-uf.jpg", "landscape-maurice-martin-oc-market-in-madagascar-uf-25-5x31-5-7513-600.jpg", "landscape-maurice-martin6052-f-1913-1974-s-21x28-the-olive-grove.jpg", "landscape-maurice-martin752-f-1913-1974-s-21x25-canal-at-episy.jpg", "landscape-vidal-2628-18x21-5.jpg", "landscape-w-lister-countryside-14x18-7811-300-uf.jpg", "seascape-2548-ed-mandon.jpg", "seascape-2614-18x24-paul-blaine-henry.jpg", "seascape-2621-size-name.jpg", "seascape-2711-aminomi-24x36.jpg", "seascape-4913-24x36-a-dzigurski.jpg", "seascape-aldo-fishing-boats-at-rest-oc-23-5x31-5-uf-2621-400.jpg", "seascape-dsc09512.jpg", "seascape-edouard-mandon-fr-franch-soleil-levant-toulon-france-17-5x29-oc-2548-200.jpg", "seascape-h-somers-la-plauge-oc-9x10-75-80103-200.jpg", "seascape-h-somers.jpg", "seascape-j-esteve-the-fishing-boats-16x24-77536-700-f.jpg", "seascape-merio-ameglio-1895-s-18x21-la-darse.jpg", "sketches-cohen-after-call6001629537703522109.jpg", "sketches-cohen-artist-wife-2215768381013609725.jpg", "sketches-cohen-artist-wife3003420913007357091.jpg", "sketches-cohen-breakdown1701758532642555186.jpg", "sketches-cohen-confusion9136425111968479270.jpg", "sketches-cohen-movement6073230065130688366.jpg", "sketches-cohen-the-call3921629793921676692.jpg", "sketches-cohen-the-matchmaker5495913992969418736.jpg", "sketches-image-104414434602694682217.jpg", "sketches-image-115046930553399839289.jpg", "sketches-image-11832540208548213473.jpg", "sketches-image-128241921762307798448.jpg", "sketches-image-147412519450228274087.jpg", "sketches-image-15490824894727614627.jpg", "sketches-image-26769692222170830039.jpg", "sketches-image-34322798448223958894.jpg", "sketches-image-44262207972820302359.jpg", "sketches-image-57304970383126218914.jpg", "sketches-image-62182427422780434193.jpg", "sketches-image-79141563538999310731.jpg", "sketches-image-85965030076614980537.jpg", "sketches-image-96055232399665535170.jpg", "sketches-image0-16772493638587363282.jpg", "sketches-image0959382791482177663.jpg", "sketches-image2878933046184001736.jpg", "sketches-solomon-cohen.jpg", "stilllife-2494-23x28-daniel-girard.jpg", "stilllife-6012-20x24-d-lartigue.jpg", "stilllife-8099-15x18-jacques-blanchard.jpg", "stilllife-amiri-88129-9x12.jpg", "stilllife-andres-segovia-ob-still-life-with-watermelon-25-25-x-31-75-77604-500.jpg", "stilllife-cortes-still-life-with-lamp-21-5x15-oil.jpg", "stilllife-daniel-gerard-oc-an-autumn-still-life-23-5x28-5-uf-2498-400.jpg", "stilllife-daniel-girard-2484-s-10x14the-fruit-bowl.jpg", "stilllife-daniel-girard-2486-s10x14-the-yellow-cup.jpg", "stilllife-daniel-girard-2498-s-10x14-the-green-tea-pot.jpg", "stilllife-daniel-girard-80138-s-10x14-the-red-tray.jpg", "stilllife-dany-lartigue-raisins-et-anemones-18x24-6012-1400-uf.jpg", "stilllife-fred-sexton-77596-s-15x18-stil-life-git-of-david-sokol-to-lacm-1.jpg", "stilllife-hollo-nelly-forsythia-and-chess-24x30-1715-900-uf.jpg", "stilllife-roger-escudie-887-s-20x24.jpg"];
    //    $scope.artcollection = [{imageurl:"1192-23x31-gyula-gero.jpg",artist:"Ferrari",title:"Landscape",medium:"Oil on Canvas"}];
                             
                             
});
app.directive('launchEvents', function () {
    return function (scope) {
        scope.$emit('eventlaunch');
    };
});
//
//app.directive('myDirective', function() {
//  return function(scope, element, attrs) {
//    if (scope.$last)
//      setTimeout(function() {
//        scope.$emit('onRepeatLast', element, attrs);
//    }, 100); 
//  };
//});
